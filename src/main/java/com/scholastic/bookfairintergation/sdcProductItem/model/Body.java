package com.scholastic.bookfairintergation.sdcProductItem.model;

import java.util.Date;

public class Body
{
  private String objectKey;

  public String getObjectKey() {
	  return this.objectKey;
	  }

  public void setObjectKey(String objectKey) { 
	  this.objectKey = objectKey; 
	  }

  private String contentDescription;

  public String getContentDescription() { 
	  return this.contentDescription; 
	  }

  public void setContentDescription(String contentDescription) { 
	  this.contentDescription = contentDescription;
	  }

  private long sizeBytes;

 

  public long getSizeBytes() {
	return sizeBytes;
}

public void setSizeBytes(long sizeBytes) {
	this.sizeBytes = sizeBytes;
}

private int recordCount;

  public int getRecordCount() {
	  return this.recordCount; 
	  }

  public void setRecordCount(int recordCount) { 
	  this.recordCount = recordCount; 
	  }

  private String lastModified;

  public String getLastModified() { 
	  return this.lastModified; 
	  }

  public void setLastModified(String newdate) { 
	  this.lastModified = newdate; 
	  }

  private Location location;

  public Location getLocation() { 
	  return this.location; 
	  }

  public void setLocation(Location location) { 
	  this.location = location; 
	  }
}

