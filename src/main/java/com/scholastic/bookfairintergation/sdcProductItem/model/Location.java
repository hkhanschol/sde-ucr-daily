package com.scholastic.bookfairintergation.sdcProductItem.model;

public class Location
{
  private String provider;

  public String getProvider() {
	  return this.provider;
	  }

  public void setProvider(String provider) { 
	  this.provider = provider; 
	  }

  private String service;

  public String getService() {
	  return this.service; 
	  }

  public void setService(String service) { 
	  this.service = service; 
	  }

  private String instance;

  public String getInstance() {
	  return this.instance;
	  }

  public void setInstance(String instance) { 
	  this.instance = instance; 
	  }
}

