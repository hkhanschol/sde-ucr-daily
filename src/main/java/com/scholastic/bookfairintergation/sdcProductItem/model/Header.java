package com.scholastic.bookfairintergation.sdcProductItem.model;

import java.util.Date;

public class Header
{
  private String messageEvent;

  public String getMessageEvent() { 
	  return this.messageEvent;
	  }

  public void setMessageEvent(String messageEvent) { 
	  this.messageEvent = messageEvent; 
	  }

  private String messageSource;

  public String getMessageSource() 
  { return this.messageSource; 
  }

  public void setMessageSource(String messageSource) { 
	  this.messageSource = messageSource;
	  }

  private String messageTimestamp;

  public String getMessageTimestamp() { 
	  return this.messageTimestamp; 
	  }

  public void setMessageTimestamp(String string) { 
	  this.messageTimestamp = string;
	  }

  private double messageBodyVersion;

  public double getMessageBodyVersion() {
	  return this.messageBodyVersion;
	  }

  public void setMessageBodyVersion(double messageBodyVersion) { 
	  this.messageBodyVersion = messageBodyVersion; 
	  }
}
