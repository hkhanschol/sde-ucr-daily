package com.scholastic.bookfairintergation.sdcProductItem.model;

public class Message
{
  private Header header;

  public Header getHeader() {
	  return this.header; 
	  }

  public void setHeader(Header header) {
	  this.header = header; 
	  }

  private Body body;

  public Body getBody() {
	  return this.body; 
	  }

  public void setBody(Body body) { 
	  this.body = body; 
	  }
}
