package com.scholastic.bookfairintergation.sdcProductItem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.JmsAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication(exclude = JmsAutoConfiguration.class)
//load regular Spring XML file from the classpath that contains the Camel XML DSL
@ImportResource({"classpath:META-INF/spring/camel-context.xml"})
public class Application {

	public static void main(String[] args) {
		
		SpringApplication.run(Application.class, args);
	
	}

}