package com.scholastic.bookfairintergation.sdcProductItem.config;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

@Configuration
public class AppConfiguration implements CamelContextAware {

	private CamelContext context;

	@Bean
	public AmazonS3 amazonS3Client() {
		AmazonS3 amazonS3Client = new AmazonS3Client(new DefaultAWSCredentialsProviderChain());
		return amazonS3Client;
	}

	@Override
	public CamelContext getCamelContext() {
		return this.context;
	}

	@Override
	public void setCamelContext(CamelContext context) {
		this.context = context;

	}

}
