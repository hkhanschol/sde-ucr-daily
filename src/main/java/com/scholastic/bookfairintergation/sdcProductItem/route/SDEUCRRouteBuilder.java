package com.scholastic.bookfairintergation.sdcProductItem.route;

import java.io.EOFException;
import java.net.ConnectException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.aws.s3.S3Constants;
import org.apache.camel.component.gson.GsonDataFormat;
import org.apache.camel.component.salesforce.SalesforceEndpointConfig;
import org.apache.camel.component.salesforce.api.SalesforceException;
import org.apache.camel.component.salesforce.api.dto.bulk.BatchInfo;
import org.apache.camel.component.salesforce.api.dto.bulk.ConcurrencyModeEnum;
import org.apache.camel.component.salesforce.api.dto.bulk.ContentType;
import org.apache.camel.component.salesforce.api.dto.bulk.JobInfo;
import org.apache.camel.component.salesforce.api.dto.bulk.OperationEnum;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.scholastic.bookfairintergation.sdcProductItem.model.Body;
import com.scholastic.bookfairintergation.sdcProductItem.model.Header;
import com.scholastic.bookfairintergation.sdcProductItem.model.Location;
import com.scholastic.bookfairintergation.sdcProductItem.model.Message;
import com.scholastic.bookfairintergation.sdcProductItem.util.DateTimeConversion;

@Component
public class SDEUCRRouteBuilder extends RouteBuilder{
	public static String job_id="";
	public int rowCount;
	public static String batch_id="";
	private final Environment properties;
	Message rootMsg;
	Body body;
	Header header;
	Location location;
	Date fileLastModified;
	long fileSizeinBytes;
	String Id = "";
	String LastModifiedDate = "";
	String LastModifiedById = "";
	String CreatedDate = "";
	String CreatedById = "";

	String Fair_ID__c = "";
	String Refused_Delivery__c = "";
	String Refused_Pickup__c = "";
	String Request_Type__c = "";
	String SBF_UCR_Cost__c = "";

	String headerValue="";

	public SDEUCRRouteBuilder(CamelContext context, Environment environment) {
		super(context);
		this.properties = environment;
	}
	
	public final Logger LOGGER = LoggerFactory.getLogger(SDEUCRRouteBuilder.class);	
	@Override
	public void configure() throws Exception {
		
		GsonDataFormat jsonNotification = new GsonDataFormat();			
		jsonNotification.setUnmarshalType(Message.class); 
		String bucketName=properties.getProperty("bucketName");
		String awsdailyFileName=properties.getProperty("awsDailyFileName");
		 
		onException(SalesforceException.class).handled(true)
		.maximumRedeliveries("{{maxRetrySalesforceException}}")
		.redeliveryDelay("{{maximumRedeliveriesSalesforceException}}")
				.process(new Processor() {
		    public void process(Exchange exchange) throws Exception
		    {	
		    	LOGGER.info("SDEUCRdaily:Exception occured of type Salesforce ,retried maximum of "+exchange.getContext().resolvePropertyPlaceholders("{{maxRetrySalesforceException}}")+" but failed");

		    }
		})
		.to("log:SDEUCRdaily-SalesforceException?level=ERROR&showCaughtException=true&showStackTrace=true");

		onException(EOFException.class).handled(true)
		.maximumRedeliveries("{{maxRetryEOFException}}")
		.redeliveryDelay("{{maximumRedeliveriesEOFException}}")
		.process(new Processor() {
		    public void process(Exchange exchange) throws Exception
		    {	
		    	LOGGER.info("SDEUCRdaily:Exception occured of type Salesforce ,retried maximum of "+exchange.getContext().resolvePropertyPlaceholders("{{maxRetryEOFException}}")+" but failed");
		    }
		})
		.to("log:SDEUCRdaily-EOFException?level=ERROR&showCaughtException=true&showStackTrace=true");
		
		onException(NoSuchElementException.class).handled(true)
		.maximumRedeliveries("{{maxRetryNoSuchElementException}}")
		.redeliveryDelay("{{maximumRedeliveriesNoSuchElementException}}")
		.process(new Processor() {
		    public void process(Exchange exchange) throws Exception
		    {	
		    	LOGGER.info("SDEUCRdaily:Exception occured of type Salesforce Batch Query ,retried maximum of "+exchange.getContext().resolvePropertyPlaceholders("{{maxRetryNoSuchElementException}}")+" but failed");
		    }
		})
		.to("log:SDEUCRdaily-NoSuchElementException?level=ERROR&showCaughtException=true&showStackTrace=true");
		
		
		onException(ConnectException.class).handled(true)
		.maximumRedeliveries("{{maxRetryConnectException}}")
		.redeliveryDelay("{{maximumRedeliveriesConnectException}}")
		.process(new Processor() {
		    public void process(Exchange exchange) throws Exception
		    {	
		    	LOGGER.info("SDEUCRdaily:Exception occured of type General ,retried maximum of "+exchange.getContext().resolvePropertyPlaceholders("{{maxRetryConnectException}}")+" but failed");
		    }
		})
		.to("log:SDEUCRdaily-ConnectException?level=ERROR&showCaughtException=true&showStackTrace=true");
		
		onException(Exception.class).handled(true)
		.maximumRedeliveries("{{maxRetryException}}")
		.redeliveryDelay("{{maximumRedeliveriesException}}")
		.process(new Processor() {
			
			@Override
			public void process(Exchange exchange) throws Exception {
				
				LOGGER.info("SDEUCRdaily:Exception occured of type General ,retried maximum of "+exchange.getContext().resolvePropertyPlaceholders("{{maxRetryException}}")+" but failed");
				
			}
		})
		.to("log:SDEUCRdaily-GeneralException?level=ERROR&showCaughtException=true&showStackTrace=true")
		.end();
		
		 ///begining of Route
		from(properties.getProperty("batchschedulerendpointDaily")).routeId("SDEUCRDR1")
		.log(">>>SDEUCR daily Route started at ${date:now:yyyyMMddHHmm}")
		.process(new Processor() {

		    public void process(Exchange exchange) throws Exception
		    {
		    	JobInfo jobInfo = new JobInfo();
		        jobInfo.setContentType(ContentType.CSV);
		        jobInfo.setOperation(OperationEnum.QUERY);
		        jobInfo.setObject("SBF_Urgent_Change_Request__c");
		        jobInfo.setConcurrencyMode(ConcurrencyModeEnum.PARALLEL);
		        exchange.getOut().setBody(jobInfo);
		        
		    }

		})
		.to("salesforce:createJob")
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				  JobInfo jobinfo = exchange.getIn().getBody(JobInfo.class);
			        job_id=jobinfo.getId();
			        exchange.getOut().setBody(job_id);
			}
		})
		.log(">>>Job id: ${body}")
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSSSSS");
			        Date date = new Date();
			        Date endDate = new DateTime(date).minusDays(1).toDate();
					Date startDate = new DateTime(date).minusDays(7).toDate();
					String startDateString = dateFormat.format(startDate).substring(0,10);
					String endDateString = dateFormat.format(endDate).substring(0,10);
					startDateString=startDateString+"T00:00:00.000Z";
					endDateString=endDateString+"T23:59:59.000Z";
			        
			        String query ="SELECT Id,LastModifiedDate,LastModifiedById,CreatedDate,CreatedById, Fair_ID__c, Refused_Delivery__c,Refused_Pickup__c,Request_Type__c,SBF_UCR_Cost__c FROM SBF_Urgent_Change_Request__c where LastModifiedDate>="+startDateString+" and LastModifiedDate<="+endDateString;
				    exchange.getOut().setBody(query);
			}
		})
		
		.process(new Processor() {

		    public void process(Exchange exchange) throws Exception
		    {	
		        exchange.setOut(exchange.getIn());
		        exchange.getOut().setHeader(SalesforceEndpointConfig.JOB_ID,job_id);
		        exchange.getOut().setHeader(SalesforceEndpointConfig.CONTENT_TYPE,ContentType.CSV);
		    }
		})
		.to("salesforce:createBatchQuery")
		.delay(Integer.parseInt(properties.getProperty("delayDaily")))
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				exchange.getOut().setBody(job_id);
			}
		})
		.log(">>>SDEUCR daily Job fired at salesforce at ${date:now:yyyyMMddHHmm}")
		.process(new Processor() {

		    public void process(Exchange exchange) throws Exception
		    {	
		        JobInfo jobInfo = new JobInfo();
		        jobInfo.setId(exchange.getIn().getBody(String.class));
		        exchange.getOut().setBody(jobInfo);

		    }

		})
		.to("salesforce:closeJob")
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				exchange.getOut().setBody(job_id);
			}
		})
		
		.process(new Processor() {

		    public void process(Exchange exchange) throws Exception
		    {	
		        JobInfo jobInfo = new JobInfo();
		        jobInfo.setId(exchange.getIn().getBody(String.class));
		        exchange.getOut().setBody(jobInfo);

		    }

		})
		.to("salesforce:getAllBatches")
		.split(body())
		.process(new Processor() {

			@Override
			public void process(Exchange exchange) throws Exception {
				
				BatchInfo obj = exchange.getIn().getBody(BatchInfo.class);
				exchange.getOut().setBody(obj);
				job_id=obj.getJobId();
				batch_id=obj.getId();
			}
			
		})
		.to("salesforce:getQueryResultIds")
		.process(new Processor(){

			@SuppressWarnings("rawtypes")
			@Override
			public void process(Exchange exchange) throws Exception {
				
				Collection resultIds = exchange.getIn().getBody(Collection.class);
		        String resultId = (String) resultIds.iterator().next();
		        exchange.getOut().setHeader(SalesforceEndpointConfig.RESULT_ID, resultId);
		        exchange.getOut().setHeader(SalesforceEndpointConfig.JOB_ID, job_id);
		        exchange.getOut().setHeader(SalesforceEndpointConfig.BATCH_ID, batch_id);
		        exchange.getOut().setBody(exchange.getIn().getBody());
			}
			
		})
		.to("salesforce:getQueryResult")
		.unmarshal().csv()
		.process(new Processor() {

		    public void process(Exchange exchange) throws Exception
		    {
		    	
		    	String outputBodyToS3="";
		    	StringBuffer strbuff=new StringBuffer();
		    	List<List<String>> data = (List<List<String>>) exchange.getIn().getBody();
		    	rowCount=data.size()-1;
		    	int i=0;
		    	if(data.size()>1){
		    	for (List<String> line : data) {
		    		

		    		boolean canInsert=true;
		    		 
		    		 Id = line.get(0);
		    		 if(Id.equals("Id")){
    					 canInsert=false;
    				 }
		    		 if(canInsert){
    				 LastModifiedDate = line.get(1);
    				 LastModifiedById = line.get(2);
    				 CreatedDate = line.get(3);
    				 CreatedById = line.get(4);
    				
    				 Fair_ID__c = line.get(5);
    					
    					Fair_ID__c = Fair_ID__c.replaceAll("\\r|\\n", " ");
    					Fair_ID__c = Fair_ID__c.replaceAll("\"", "");
    					Fair_ID__c = checkIfOnlyNumber(Fair_ID__c);
    				    				
    				 Refused_Delivery__c = line.get(6);
    				
    					Refused_Delivery__c = Refused_Delivery__c.replaceAll("\\r|\\n", " ");
    					Refused_Delivery__c = Refused_Delivery__c.replaceAll("\"", "");
    					Refused_Delivery__c = checkIfOnlyNumber(Refused_Delivery__c);

    				Refused_Pickup__c = line.get(7);

    					Refused_Pickup__c = Refused_Pickup__c.replaceAll("\\r|\\n", " ");
    					Refused_Pickup__c = Refused_Pickup__c.replaceAll("\"", "");
    					Refused_Pickup__c = checkIfOnlyNumber(Refused_Pickup__c);

    				Request_Type__c = line.get(8);

    					Request_Type__c = Request_Type__c.replaceAll("\\r|\\n", " ");
    					Request_Type__c = Request_Type__c.replaceAll("\"", "");
    					Request_Type__c = checkIfOnlyNumber(Request_Type__c);

    				SBF_UCR_Cost__c = line.get(9);

    					SBF_UCR_Cost__c = SBF_UCR_Cost__c.replaceAll("\\r|\\n", " ");
    					SBF_UCR_Cost__c = SBF_UCR_Cost__c.replaceAll("\"", "");
    					SBF_UCR_Cost__c = checkIfOnlyNumber(SBF_UCR_Cost__c);
    				
    				
    					strbuff.append(Id+"|");
    					strbuff.append(LastModifiedDate+"|");
    					strbuff.append(LastModifiedById+"|");
    					strbuff.append(CreatedDate+"|");
    					strbuff.append(CreatedById+"|");

    					strbuff.append(Fair_ID__c+"|");
    					strbuff.append(Refused_Delivery__c+"|");
    					strbuff.append(Refused_Pickup__c+"|");
    					strbuff.append(Request_Type__c+"|");

    					strbuff.append(SBF_UCR_Cost__c+"\n");
		    		
		    	}
		    	}
		    	
		    }else{

	    		outputBodyToS3="No Records Found";
	    	
		    }
		    	headerValue="Id|LastModifiedDate|LastModifiedById|CreatedDate|CreatedById|Fair_ID__c|Refused_Delivery__c|Refused_Pickup__c|Request_Type__c|SBF_UCR_Cost__c";
		    	StringBuffer strbufffinal=new StringBuffer();
		    	 strbufffinal.append(headerValue+"\n"+strbuff.toString());
		    	 exchange.getOut().setBody(strbufffinal.toString());	
		        
		    }
		})
		.marshal().gzip()
		.setHeader(S3Constants.KEY, simple(awsdailyFileName))
		.to("aws-s3://" + bucketName + "?amazonS3Client=#amazonS3Client")
		.log(">>>SDEUCR daily Files sent to AWS S3 at ${date:now:yyyyMMddHHmm}")
		.delay(10000)
		.process(new Processor() {
			
			public void process(Exchange exchange) throws Exception {
				
				AmazonS3 s3 = (AmazonS3) exchange.getContext().getRegistry().lookup("amazonS3Client");
				 
				try {
					S3Object object = s3.getObject(bucketName, awsdailyFileName);
					fileSizeinBytes = object.getObjectMetadata().getContentLength();
					fileLastModified = object.getObjectMetadata().getLastModified();
				} catch (Exception e) {

				}
				DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");
				String lastModifiedDate=targetFormat.format(fileLastModified).toString();
				
				
				
				rootMsg=new Message();
				body=new Body();
				header= new Header();
				location=new Location();
				
				location.setProvider("aws");
				location.setService("s3");
				location.setInstance(properties.getProperty("bucketName"));
				
				body.setObjectKey("/"+awsdailyFileName);
				body.setContentDescription("Product Item attributes");
				body.setSizeBytes(fileSizeinBytes);
				body.setRecordCount(rowCount);
				
				body.setLastModified(lastModifiedDate);
				body.setLocation(location);
				
				header.setMessageEvent("ObjectPublished");
				header.setMessageSource("Salesforce");
				header.setMessageTimestamp(new DateTimeConversion().getCurrentDateinSFDCFormat());
				header.setMessageBodyVersion(1.0);
				
				rootMsg.setBody(body);
				rootMsg.setHeader(header);
				
				exchange.getOut().setBody(rootMsg);
			
			}
		})
		.marshal(jsonNotification)
		.convertBodyTo(String.class)
		.log("${body}")
		.to(properties.getProperty("sdeTopicName"))
		.log(">>>SDEUCR daily JSON file sent to Kafka at ${date:now:yyyyMMddHHmm}")
		.end();
		
		
		
		
		
	

}
	private String checkIfOnlyNumber(String instance){
		if(instance.length()==0){
			return "";
		}else if(instance.matches("[0-9]+")){
			return instance;
		}else{
			instance="\""+instance+"\"";
			return instance;
		}
		
		
		
		
	}
}
