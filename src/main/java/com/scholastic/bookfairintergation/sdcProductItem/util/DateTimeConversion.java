package com.scholastic.bookfairintergation.sdcProductItem.util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeConversion {
	
	public String startDateRange() throws Exception{
		Calendar c1= Calendar.getInstance();
		c1.add(Calendar.DATE, -7);  
	 	c1.set(Calendar.HOUR_OF_DAY, 0);
		c1.set(Calendar.MINUTE, 0);
		c1.set(Calendar.SECOND, 0);
	    return  convertDateTimeToSFDCFormat(convertDateToyyyyMMddhhmmssformat(c1.getTime()));	
	}
	
	public String endDateRange() throws Exception{
		Calendar c1= Calendar.getInstance();
		c1.add(Calendar.DATE,-1); 
	 	c1.set(Calendar.HOUR_OF_DAY, 23);
		c1.set(Calendar.MINUTE, 59);
		c1.set(Calendar.SECOND, 59);
		return  convertDateTimeToSFDCFormat(convertDateToyyyyMMddhhmmssformat(c1.getTime()));
	}
	
	public String convertDateToyyyyMMddhhmmssformat(Date dateString) throws Exception{
		DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = targetFormat.format(dateString); 
		return formattedDate;	
		}
	public String convertDateTimeToSFDCFormat(String dateString) throws Exception{
		DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");
		Date date = originalFormat.parse(dateString);
		String formattedDate = targetFormat.format(date); 
	return formattedDate;	
	}
	
	public String getCurrentDateinSFDCFormat()throws Exception{
		 	Calendar c2= Calendar.getInstance();
			c2.add(Calendar.DATE,0); 
			return  convertDateTimeToSFDCFormat(convertDateToyyyyMMddhhmmssformat(c2.getTime()));
	}
	

	
}
